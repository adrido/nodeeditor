#ifndef WIELDITEMMODELEDITOR_H
#define WIELDITEMMODELEDITOR_H

#include <QtGui/QMainWindow>
#include "ui_wielditemmodeleditor.h"
#include "GLWidget.h"

class WieldItemModelEditor : public QMainWindow
{
    Q_OBJECT

public slots:
	void add_button_clicked();
	void refreshTextures();
	void delete_button_clicked();
	void selection_changed();
	void handleMenu(QAction* action);
	void texture_changed();
	void textureIndexChanged(int index);
	void btn_sel_texture_clicked();
	void boxname_changed();
	void spinboxValueChanged(double);

public:
    WieldItemModelEditor(QWidget *parent = 0);
    ~WieldItemModelEditor();

private:
    bool WritePngSilouette(char* buffer, QString filename,int texturesize);
    box getbox(bool* successfull);
    void UpdateBoxView();
    Ui::WieldItemModelEditorClass ui;
    GLWidget previewarea;
    QList<box> m_currentBoxes;

    QString m_texture_filenames[6];
};

#endif // WIELDITEMMODELEDITOR_H
