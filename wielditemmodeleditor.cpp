#include "wielditemmodeleditor.h"
#include <QFileDialog>
#include <png.h>
#include <assert.h>

WieldItemModelEditor::WieldItemModelEditor(QWidget *parent)
    : QMainWindow(parent)
{
	ui.setupUi(this);
	ui.vLayout_Preview_Area->insertWidget(0,&(this->previewarea),true,0);
	this->previewarea.show();
	this->previewarea.setBoxes(&m_currentBoxes);

	QObject::connect(ui.btn_add,SIGNAL(clicked()),this,SLOT(add_button_clicked()));
	QObject::connect(ui.btn_delete,SIGNAL(clicked()),this,SLOT(delete_button_clicked()));
	QObject::connect(ui.btn_sel_texture,SIGNAL(clicked()),this,SLOT(btn_sel_texture_clicked()));
	QObject::connect(ui.cb_texture_mode,SIGNAL(currentIndexChanged(int)),this,SLOT(textureIndexChanged(int)));
	QObject::connect(ui.le_texturename,SIGNAL(editingFinished()),this,SLOT(texture_changed()));
	QObject::connect(ui.le_boxname,SIGNAL(editingFinished()),this,SLOT(boxname_changed()));
	QObject::connect(ui.menubar,SIGNAL(triggered(QAction*)),this,SLOT(handleMenu(QAction*)));
	QObject::connect(ui.lw_boxes,SIGNAL(itemSelectionChanged()),this,SLOT(selection_changed()));
	QObject::connect(ui.sb_xmin,SIGNAL(valueChanged(double)),this,SLOT(spinboxValueChanged(double)));
	QObject::connect(ui.sb_xmax,SIGNAL(valueChanged(double)),this,SLOT(spinboxValueChanged(double)));
	QObject::connect(ui.sb_ymin,SIGNAL(valueChanged(double)),this,SLOT(spinboxValueChanged(double)));
	QObject::connect(ui.sb_ymax,SIGNAL(valueChanged(double)),this,SLOT(spinboxValueChanged(double)));
	QObject::connect(ui.sb_zmin,SIGNAL(valueChanged(double)),this,SLOT(spinboxValueChanged(double)));
	QObject::connect(ui.sb_zmax,SIGNAL(valueChanged(double)),this,SLOT(spinboxValueChanged(double)));
	QObject::connect(ui.cb_floor,SIGNAL(stateChanged(int)),&(this->previewarea),SLOT(setFloorGridEnabled(int)));
	QObject::connect(ui.pb_RefreshTextures,SIGNAL(clicked()),this,SLOT(refreshTextures()));
}

WieldItemModelEditor::~WieldItemModelEditor()
{

}

box WieldItemModelEditor::getbox(bool* successfull) {
	box toadd;
	bool ok = true;

	toadd.name = this->ui.le_boxname->text();

	if (this->ui.sb_xmin->value() < this->ui.sb_xmax->value()) {
		toadd.x_min = this->ui.sb_xmin->value();
		toadd.x_max = this->ui.sb_xmax->value();
	}
	else {
		//TODO show error message
		ok = false;
	}

	if (this->ui.sb_ymin->value() < this->ui.sb_ymax->value()) {
		toadd.y_min = this->ui.sb_ymin->value();
		toadd.y_max = this->ui.sb_ymax->value();
	}
	else {
		//TODO show error message
		ok = false;
	}

	if (this->ui.sb_zmin->value() < this->ui.sb_zmax->value()) {
		toadd.z_min = this->ui.sb_zmin->value();
		toadd.z_max = this->ui.sb_zmax->value();
	}
	else {
		//TODO show error message
		ok = false;
	}

	*successfull = ok;
	return toadd;
}

void WieldItemModelEditor::add_button_clicked() {

	if (this->ui.le_boxname->text() == "") {
		this->ui.statusbar->showMessage("NOT adding box without name!",1500);
		return;
	}

	bool found = false;
	int selectedbox = -1;
	int loopcount = 0;

	for (QList<box>::iterator i = m_currentBoxes.begin(); i != m_currentBoxes.end(); i++) {
		if ((*i).name == this->ui.le_boxname->text()) {
			bool conversion_ok;
			box toadd = getbox(&conversion_ok);

			if (conversion_ok) {
				(*i).x_max = toadd.x_max;
				(*i).y_max = toadd.y_max;
				(*i).z_max = toadd.z_max;
				(*i).x_min = toadd.x_min;
				(*i).y_min = toadd.y_min;
				(*i).z_min = toadd.z_min;
			}
			selectedbox = loopcount;
			found = true;
			break;
		}
		loopcount ++;
	}

	if (!found) {
		selectedbox = this->m_currentBoxes.count();
		bool conversion_ok = true;
		box toadd = getbox(&conversion_ok);

		if (conversion_ok)
			m_currentBoxes.push_back(toadd);
		else
			this->ui.statusbar->showMessage("NOT adding box! Invalid box parameters!",1500);

	}

	//update listview
	UpdateBoxView();
	if (selectedbox != -1)
		this->ui.lw_boxes->setCurrentRow(selectedbox);
	this->previewarea.updateGL();
}

void WieldItemModelEditor::delete_button_clicked() {
	if (this->ui.le_boxname->text() == "") {
		this->ui.statusbar->showMessage("Can't delete box without name!",1500);
		return;
	}

	int pos = 0;
	for (QList<box>::iterator i = m_currentBoxes.begin(); i != m_currentBoxes.end(); i++) {
			if ((*i).name == this->ui.le_boxname->text()) {
				m_currentBoxes.removeAt(pos);
				break;
			}
			pos++;
	}
	//update listview
	UpdateBoxView();

	this->previewarea.updateGL();

	this->ui.le_boxname->setText("");
	this->ui.sb_xmax->setValue(0);
	this->ui.sb_xmin->setValue(0);
	this->ui.sb_ymax->setValue(0);
	this->ui.sb_ymin->setValue(0);
	this->ui.sb_zmax->setValue(0);
	this->ui.sb_zmin->setValue(0);


	this->ui.le_texturename->setText("");
	this->ui.cb_texture_mode->setCurrentIndex(0);
}

void WieldItemModelEditor::selection_changed() {

	int row = this->ui.lw_boxes->currentRow();

	if (m_currentBoxes.count() > row) {
		double zmin = m_currentBoxes[row].z_min;
		double zmax = m_currentBoxes[row].z_max;
		double xmin = m_currentBoxes[row].x_min;
		double xmax = m_currentBoxes[row].x_max;
		double ymin = m_currentBoxes[row].y_min;
		double ymax = m_currentBoxes[row].y_max;

		this->ui.le_boxname->setText(this->m_currentBoxes[row].name);
		this->ui.sb_xmax->setValue(xmax);
		this->ui.sb_xmin->setValue(xmin);
		this->ui.sb_ymax->setValue(ymax);
		this->ui.sb_ymin->setValue(ymin);
		this->ui.sb_zmax->setValue(zmax);
		this->ui.sb_zmin->setValue(zmin);

		this->previewarea.setSelectedBox(this->ui.le_boxname->text());
		this->previewarea.updateGL();
	}
}

inline void setRGB(char* buffer, int r, int g, int b) {
	buffer[0] = r;
	buffer[1] = g;
	buffer[2] = b;
}

void WieldItemModelEditor::handleMenu(QAction* action) {

	if (action->iconText() == "close") {
		this->m_currentBoxes.clear();
		this->ui.le_boxname->setText("");
		this->ui.sb_xmax->setValue(0);
		this->ui.sb_xmin->setValue(0);
		this->ui.sb_ymax->setValue(0);
		this->ui.sb_ymin->setValue(0);
		this->ui.sb_zmax->setValue(0);
		this->ui.sb_zmin->setValue(0);
		this->ui.lw_boxes->clear();
	}

	if (action->iconText() == "save") {
		QString filename = QFileDialog::getSaveFileName(this, "Save file", "", "Model Files (*.wim)");

		if (filename != "") {

			QFile tosaveto(filename);

			if (tosaveto.open(QIODevice::Truncate | QIODevice::WriteOnly )) {
				QString firstline = this->ui.le_modelname->text()
									+ ";" + this->m_texture_filenames[0] +
									+ ";" + this->m_texture_filenames[1] +
									+ ";" + this->m_texture_filenames[2] +
									+ ";" + this->m_texture_filenames[3] +
									+ ";" + this->m_texture_filenames[4] +
									+ ";" + this->m_texture_filenames[5] +
									+ ";" + this->ui.le_texture_size->text() + "\n";
				tosaveto.write(firstline.toAscii());

				for (QList<box>::iterator i = m_currentBoxes.begin(); i != m_currentBoxes.end(); i++) {
					QString line = (*i).name + ";" + QString::number((*i).x_min,'f',10)
											 + ";" + QString::number((*i).y_min,'f',10)
											 + ";" + QString::number((*i).z_min,'f',10)
											 + ";" + QString::number((*i).x_max,'f',10)
											 + ";" + QString::number((*i).y_max,'f',10)
											 + ";" + QString::number((*i).z_max,'f',10) + "\n";

					tosaveto.write(line.toAscii());
					}
				tosaveto.close();
			}
			else
				this->ui.statusbar->showMessage("Failed to open file for saving: " + filename,1500);
		}
	}

	if (action->iconText() == "open") {
		QString filename = QFileDialog::getOpenFileName(this, "Open file", "", "Model Files (*.wim)");

		if (filename != "") {
			this->m_currentBoxes.clear();
			this->ui.le_boxname->setText("");
			this->ui.sb_xmax->setValue(0);
			this->ui.sb_xmin->setValue(0);
			this->ui.sb_ymax->setValue(0);
			this->ui.sb_ymin->setValue(0);
			this->ui.sb_zmax->setValue(0);
			this->ui.sb_zmin->setValue(0);
			this->ui.lw_boxes->clear();

			QFile readfrom(filename);

			if (readfrom.open(QIODevice::ReadOnly )) {
				QString line = "";
				bool firstline = true;

				while (!readfrom.atEnd()) {
					line = readfrom.readLine(8192);

					if (firstline) {
						QStringList parts = line.split(';');
						this->ui.le_modelname->setText(parts[0]);

						for (int i=1;i<7;i++) {
							if ( parts.count() > i) {
								this->m_texture_filenames[i-1] = parts[i];
								this->previewarea.loadTexture(parts[i],i-1);
							}
						}
						this->ui.le_texture_size->setText(parts[7]);
						this->ui.le_texturename->setText(this->m_texture_filenames[0]);
						this->ui.cb_texture_mode->setCurrentIndex(0);
						firstline = false;
					}
					else {
						QStringList parts = line.split(';');
						bool conversion_successfull = true;
						if (parts.count() == 7) {
							box toadd;

							toadd.name = parts[0];

							toadd.x_min = parts[1].toFloat(&conversion_successfull);

							if (conversion_successfull)
								toadd.y_min = parts[2].toFloat(&conversion_successfull);
							if (conversion_successfull)
								toadd.z_min = parts[3].toFloat(&conversion_successfull);
							if (conversion_successfull)
								toadd.x_max = parts[4].toFloat(&conversion_successfull);
							if (conversion_successfull)
								toadd.y_max = parts[5].toFloat(&conversion_successfull);
							if (conversion_successfull)
								toadd.z_max = parts[6].toFloat(&conversion_successfull);

							if (conversion_successfull) {
								this->m_currentBoxes.push_back(toadd);
							}
						}
					}
				}

				UpdateBoxView();
				this->previewarea.updateGL();
			}
			else
			this->ui.statusbar->showMessage("Failed to open file: " + filename,1500);
		}
	}

	if (action->iconText() == "export to Lua") {

		QString texsize = this->ui.le_texture_size->text();
		bool converted;
		int texturesize = texsize.toInt(&converted);

		if (converted) {
			QString luastring = "function x(value)\n\t return (value - (" + texsize + "/2)) / " + texsize + "\nend\n\n";

			luastring = luastring + "function z(value)\n\t return (value - (" + texsize + "/2)) / " + texsize + "\nend\n\n";
			luastring = luastring + "function y(value)\n\t return (value + (" + texsize + "/2)) / " + texsize + "\nend\n\n";

			luastring = luastring + "local nodebox_" + this->ui.le_modelname->text() + " = {\n";

			for (QList<box>::iterator i = this->m_currentBoxes.begin(); i != this->m_currentBoxes.end(); i++) {

				luastring = luastring + "\t--" + (*i).name + "\n";
				luastring = luastring + "\t{ x(" + QString::number((*i).x_min/BS * texturesize +(texturesize/2),'f',3) + "),"
											"y(" + QString::number((*i).y_max/BS * texturesize -(texturesize/2),'f',3)+ ")," +
											"z(" + QString::number((*i).z_max/BS * texturesize +(texturesize/2),'f',3)+ ")," +
											"\n\t\tx(" + QString::number((*i).x_max/BS * texturesize +(texturesize/2),'f',3)+ ")," +
											"y(" + QString::number((*i).y_min/BS * texturesize -(texturesize/2),'f',3)+ ")," +
											"z(" + QString::number((*i).z_min/BS * texturesize +(texturesize/2),'f',3)+ ") },\n";
			}
			luastring = luastring + "}\n";
			this->ui.plainTextEdit->setPlainText(luastring);
		}
		else
		this->ui.statusbar->showMessage("Invalid texture size",1500);
	}

	if (action->iconText() == "create texture templates") {
		if (this->ui.le_texture_size->text() != "") {

			QString directory = QFileDialog::getExistingDirectory(this, tr("Open Directory"),"",QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

			if (directory != "") {
				bool converted = false;

				int texturesize = this->ui.le_texture_size->text().toInt(&converted,10);

				if (converted) {
					//ask for folder

					//allocate memory
					char* buffer_front = (char*) malloc(texturesize * texturesize * 3 * sizeof(char));
					char* buffer_side  = (char*) malloc(texturesize * texturesize * 3 * sizeof(char));
					char* buffer_top   = (char*) malloc(texturesize * texturesize * 3 * sizeof(char));

					//fill with background
					for (int i=0; i < texturesize; i++) {
						char* row_side = &buffer_front[i*texturesize*3];
						char* row_front = &buffer_side[i*texturesize*3];
						char* row_top = &buffer_top[i*texturesize*3];
						for (int j = 0; j < texturesize*3; j+=3) {

							setRGB(&row_side[j], 200, 0, 255);
							setRGB(&row_front[j], 200, 0, 255);
							setRGB(&row_top[j], 200, 0, 255);
						}
					}

					//loop through boxes
					for (QList<box>::iterator i = this->m_currentBoxes.begin(); i != this->m_currentBoxes.end(); i++) {

						int wmin = 0;
						int wmax = 0;
						int hmin = 0;
						int hmax = 0;

						//prepare for side shilouette
						if ((*i).x_min >= -(BS/2))
							wmin = REL_XZ((*i).x_min) * texturesize;
						else if ((*i).x_min < (BS/2))
							wmin = 0;

						if ((*i).x_max <= (BS/2))
							wmax =  REL_XZ((*i).x_max) * texturesize;
						else if ((*i).x_max > -(BS/2))
							wmax = texturesize;

						if ((*i).y_min >= -(BS/2))
							hmin = (((BS/2) + (*i).y_min) / BS) * texturesize;
						else if ((*i).y_min < (BS/2))
							hmin = 0;

						if ((*i).y_max <= (BS/2))
							hmax = (((BS/2) + (*i).y_max) / BS) * texturesize;
						else if ((*i).y_max > -(BS/2))
							hmax = texturesize;


						for (int w = wmin ; w < wmax; w++) {
							for (int h= hmin+1; h <= hmax; h++) {
								char colorvalue = ((*i).z_min / BS) *255;
								assert(&buffer_front[(texturesize-h)*texturesize*3 + w*3] < buffer_front + texturesize * texturesize * 3 * sizeof(char));
								setRGB(&buffer_front[(texturesize-h)*texturesize*3 + w*3],colorvalue,colorvalue,colorvalue);
							}
						}

						//prepare for front shilouette
						int wmin_backup = wmin;
						int wmax_backup = wmax;
						wmin = 0;
						wmax = 0;
						//h can be reused
						if ((*i).z_min >= -(BS/2))
							wmin = REL_XZ((*i).z_min) * texturesize;
						else if ((*i).z_min < (BS/2))
							wmin = 0;

						if ((*i).z_max <= (BS/2))
							wmax =  REL_XZ((*i).z_max) * texturesize;
						else if ((*i).z_max > -(BS/2))
							wmax = texturesize;

						for (int w = wmin ; w < wmax; w++) {
							for (int h= hmin+1; h <= hmax; h++) {
								char colorvalue = ((*i).x_min / BS) *255;
								assert(&buffer_side[(texturesize-h)*texturesize*3 + w*3] < buffer_front + texturesize * texturesize * 3 * sizeof(char));
								setRGB(&buffer_side[(texturesize-h)*texturesize*3 +w*3],colorvalue,colorvalue,colorvalue);
							}
						}

						hmin = wmin;
						hmax = wmax;
						wmin = wmin_backup;
						wmax = wmax_backup;
						for (int w = wmin ; w < wmax; w++) {
							for (int h= hmin+1; h <= hmax; h++) {
								char colorvalue = ((*i).y_min / BS) *255;
								assert(&buffer_top[(texturesize-h)*texturesize*3 + w*3] < buffer_front + texturesize * texturesize * 3 * sizeof(char));
								setRGB(&buffer_top[(texturesize-h)*texturesize*3 +w*3],colorvalue,colorvalue,colorvalue);
							}
						}
					}


					WritePngSilouette(buffer_front, directory + QDir::separator() + "texture_projected_side.png",texturesize);
					WritePngSilouette(buffer_side, directory + QDir::separator() + "texture_projected_front.png",texturesize);
					WritePngSilouette(buffer_top, directory + QDir::separator() + "texture_projected_top.png",texturesize);

					free(buffer_front);
					free(buffer_side);
					free(buffer_top);
				}
			}
		}
	}
}

bool WieldItemModelEditor::WritePngSilouette(char* buffer, QString filename,int texturesize) {
	//open image
	FILE* pngfile = fopen(filename.toAscii().data(),"wb");
	if (pngfile == NULL) {
		return false;
	}

	png_structp png_ptr;
	png_infop info_ptr;

	png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (png_ptr == NULL) {
		fclose(pngfile);
		return false;
	}
	info_ptr = png_create_info_struct(png_ptr);
	if (info_ptr == NULL) {
		fclose(pngfile);
		png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
		return false;
	}

	if (setjmp(png_jmpbuf(png_ptr))) {
		fclose(pngfile);
		png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
		png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
		return false;
	}

	png_init_io(png_ptr, pngfile);

	png_set_IHDR(png_ptr, info_ptr, texturesize, texturesize,
			 8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
			 PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);


	png_color_8 sig_bit;
	sig_bit.red = 8;
	sig_bit.green = 8;
	sig_bit.blue = 8;
	sig_bit.alpha = 8;
	png_set_sBIT( png_ptr, info_ptr, &sig_bit );

	png_write_info(png_ptr, info_ptr);

	for (int i=0; i < texturesize; i++) {
		png_bytep row_pointer = (png_bytep) &(buffer[i* texturesize *3]);

		png_write_row(png_ptr, row_pointer);
	}

	//close image
	png_write_end(png_ptr, NULL);
	fclose(pngfile);
	return true;
}

void WieldItemModelEditor::texture_changed() {
	//save filename to internal datastructure
	this->m_texture_filenames[this->ui.cb_texture_mode->currentIndex()] = this->ui.le_texturename->text();

	//check if file exists

	//reload texture in model viewer
	this->previewarea.loadTexture(this->ui.le_texturename->text(),this->ui.cb_texture_mode->currentIndex());
}

void WieldItemModelEditor::textureIndexChanged(int index) {
	//load filename from internal datastructure
	this->ui.le_texturename->setText(this->m_texture_filenames[index]);
}

void WieldItemModelEditor::btn_sel_texture_clicked() {
	QString filename = QFileDialog::getOpenFileName(this, "Open file", "", "PNG Files (*.png *.PNG)");

	if (filename != "") {
		this->m_texture_filenames[this->ui.cb_texture_mode->currentIndex()] = filename;
		this->ui.le_texturename->setText(filename);
		this->previewarea.loadTexture(filename,this->ui.cb_texture_mode->currentIndex());
	}

}

void WieldItemModelEditor::boxname_changed() {
	this->previewarea.setSelectedBox(this->ui.le_boxname->text());
	this->previewarea.updateGL();
}

void WieldItemModelEditor::UpdateBoxView() {

	this->ui.lw_boxes->clear();
	for (QList<box>::iterator i = m_currentBoxes.begin(); i != m_currentBoxes.end(); i++) {
		this->ui.lw_boxes->addItem((*i).name);
		}
}

void WieldItemModelEditor::spinboxValueChanged(double) {
	//only do direct change if box already added
	for (QList<box>::iterator i = m_currentBoxes.begin(); i != m_currentBoxes.end(); i++) {
		if ((*i).name == this->ui.le_boxname->text()) {
			bool ok;
			box tochange = this->getbox(&ok);
			if (ok) {
				(*i).x_max = tochange.x_max;
				(*i).y_max = tochange.y_max;
				(*i).z_max = tochange.z_max;
				(*i).x_min = tochange.x_min;
				(*i).y_min = tochange.y_min;
				(*i).z_min = tochange.z_min;
				}
			}
			this->previewarea.updateGL();
		}
}

void WieldItemModelEditor::refreshTextures() {
	for (int i=0;i<6;i++) {
		this->previewarea.loadTexture(this->m_texture_filenames[i],i);
	}
}
