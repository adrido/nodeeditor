TEMPLATE = app
TARGET = WieldItemModelEditor 

QT        += core gui opengl

HEADERS   += wielditemmodeleditor.h \
			GLWidget.h
SOURCES   += main.cpp \
    wielditemmodeleditor.cpp \
    GLWidget.cpp
FORMS     += wielditemmodeleditor.ui    
RESOURCES +=
LIBS      += -lpng -lGLU
