#include <GL/gl.h>
#include <QtGui>
#include <QtOpenGL>

#include <math.h>
#include <stdlib.h>
#include <png.h>
#include <string.h>

#include "GLWidget.h"
#include <GL/glu.h>



bool loadPngImage(char *name, int &outWidth, int &outHeight, bool &outHasAlpha, GLubyte **outData) {
    png_structp png_ptr;
    png_infop info_ptr;
    unsigned int sig_read = 0;
    FILE *fp;

    if ((fp = fopen(name, "rb")) == NULL)
        return false;

    /* Create and initialize the png_struct
     * with the desired error handler
     * functions.  If you want to use the
     * default stderr and longjump method,
     * you can supply NULL for the last
     * three parameters.  We also supply the
     * the compiler header file version, so
     * that we know if the application
     * was compiled with a compatible version
     * of the library.  REQUIRED
     */
    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,
            NULL, NULL, NULL);

    if (png_ptr == NULL) {
        fclose(fp);
        return false;
    }

    /* Allocate/initialize the memory
     * for image information.  REQUIRED. */
    info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL) {
        fclose(fp);
        png_destroy_read_struct(&png_ptr, 0, 0);
        return false;
    }

    /* Set error handling if you are
     * using the setjmp/longjmp method
     * (this is the normal method of
     * doing things with libpng).
     * REQUIRED unless you  set up
     * your own error handlers in
     * the png_create_read_struct()
     * earlier.
     */
    if (setjmp(png_jmpbuf(png_ptr))) {
        /* Free all of the memory associated
         * with the png_ptr and info_ptr */
        png_destroy_read_struct(&png_ptr, &info_ptr, 0);
        fclose(fp);
        /* If we get here, we had a
         * problem reading the file */
        return false;
    }

    /* Set up the output control if
     * you are using standard C streams */
    png_init_io(png_ptr, fp);

    /* If we have already
     * read some of the signature */
    png_set_sig_bytes(png_ptr, sig_read);

    /*
     * If you have enough memory to read
     * in the entire image at once, and
     * you need to specify only
     * transforms that can be controlled
     * with one of the PNG_TRANSFORM_*
     * bits (this presently excludes
     * dithering, filling, setting
     * background, and doing gamma
     * adjustment), then you can read the
     * entire image (including pixels)
     * into the info structure with this
     * call
     *
     * PNG_TRANSFORM_STRIP_16 |
     * PNG_TRANSFORM_PACKING  forces 8 bit
     * PNG_TRANSFORM_EXPAND forces to
     *  expand a palette into RGB
     */
    png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_STRIP_16 | PNG_TRANSFORM_PACKING | PNG_TRANSFORM_EXPAND, 0);

    outWidth = png_get_image_width(png_ptr, info_ptr);
    outHeight = png_get_image_height(png_ptr, info_ptr);
    switch (png_get_color_type(png_ptr,info_ptr)) {
        case PNG_COLOR_TYPE_RGBA:
            outHasAlpha = true;
            break;
        case PNG_COLOR_TYPE_RGB:
            outHasAlpha = false;
            break;
        default:
            png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
            fclose(fp);
            return false;
    }
    unsigned int row_bytes = png_get_rowbytes(png_ptr, info_ptr);
    *outData = (unsigned char*) malloc(row_bytes * outHeight);

    png_bytepp row_pointers = png_get_rows(png_ptr, info_ptr);

    for (int i = 0; i < outHeight; i++) {
        // note that png is ordered top to
        // bottom, but OpenGL expect it bottom to top
        // so the order or swapped
        memcpy(*outData+(row_bytes * (outHeight-1-i)), row_pointers[i], row_bytes);
    }

    /* Clean up after the read,
     * and free any memory allocated */
    png_destroy_read_struct(&png_ptr, &info_ptr, 0);

    /* Close the file */
    fclose(fp);

    /* That's it */
    return true;
}

 GLWidget::GLWidget(QWidget *parent)
     : QGLWidget(parent)
 {
     xRot = 500;
     yRot = 5000;
     zRot = 0;

     m_scale = 0.1;
     m_width = 0;
     m_height = 0;
     m_bFloorGridEnabled = true;
     m_bAxisEnabled = true;

     for (int i = 0; i < 6; i ++) {
    	 this->m_textures[i] = -1;
    	 this->m_texture_data[i] = 0;
     }
 }

 GLWidget::~GLWidget()
 {
     makeCurrent();
 }

 void GLWidget::setXRotation(int angle)
 {
     normalizeAngle(&angle);
     if (angle != xRot) {
         xRot = angle;
         emit xRotationChanged(angle);
         updateGL();
     }
 }

 void GLWidget::setYRotation(int angle)
 {
     normalizeAngle(&angle);
     if (angle != yRot) {
         yRot = angle;
         emit yRotationChanged(angle);
         updateGL();
     }
 }

 void GLWidget::setZRotation(int angle)
 {
     normalizeAngle(&angle);
     if (angle != zRot) {
         zRot = angle;
         emit zRotationChanged(angle);
         updateGL();
     }
 }

 void GLWidget::initializeGL()
 {
	//gl initialization
	glShadeModel(GL_SMOOTH);
    glEnable(GL_NORMALIZE);
    glEnable(GL_LINE_SMOOTH);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glEnable(GL_DEPTH_TEST);


    // initialize dummy texture
    glGenTextures( 1, &dummytexture );
    glBindTexture( GL_TEXTURE_2D, dummytexture );

    int width, height;
    bool hasAlpha;
    char filename[] = "texture_spacer.png";
    bool success = loadPngImage(filename, width, height, hasAlpha, &dummytexture_data);
    if (success) {


      glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
      glTexImage2D(GL_TEXTURE_2D, 0, hasAlpha ? 4 : 3, width,
              height, 0, hasAlpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE,
            		  dummytexture_data);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      }
 }

void GLWidget::loadTexture(QString filename, int number) {
	glEnable(GL_TEXTURE_2D);
	if (glIsTexture(this->m_textures[number])) {
		glDeleteTextures(1,&this->m_textures[number]);
	}
	if (QFile::exists(filename)) {
		glGenTextures( 1, &this->m_textures[number] );

		glBindTexture( GL_TEXTURE_2D, this->m_textures[number] );

		int width, height;
		bool hasAlpha;

		bool success = loadPngImage(filename.toAscii().data(), width, height, hasAlpha, &this->m_texture_data[number]);
		if (success) {


		  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		  glTexImage2D(GL_TEXTURE_2D, 0, hasAlpha ? 4 : 3, width,
				  height, 0, hasAlpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE,
						  this->m_texture_data[number]);
		  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		  }
	}
	else {
		//TODO error message
		this->m_textures[number] = -1;
	}

	glDisable(GL_TEXTURE_2D);
}



 void GLWidget::drawBox(box todraw) {
	 glEnable(GL_TEXTURE_2D);

	 if (todraw.name == this->m_selected_box)
		 glColor3f(1.0,0.75,0.75);
	 else
		glColor3f(1.0,1.0,1.0);

	 if (glIsTexture(this->m_textures[0]))
		 glBindTexture(GL_TEXTURE_2D,this->m_textures[0]);
	 else
		 glBindTexture(GL_TEXTURE_2D,this->dummytexture);

	 //right
     glBegin(GL_QUADS);
     glTexCoord2f(REL_XZ_INV(todraw.x_min), REL_Y(todraw.y_max));
     glVertex3f(todraw.x_min,todraw.y_max,todraw.z_max);
     glTexCoord2f(REL_XZ_INV(todraw.x_min), REL_Y(todraw.y_min));
     glVertex3f(todraw.x_min,todraw.y_min,todraw.z_max);
     glTexCoord2f(REL_XZ_INV(todraw.x_max),REL_Y(todraw.y_min));
     glVertex3f(todraw.x_max,todraw.y_min,todraw.z_max);
     glTexCoord2f(REL_XZ_INV(todraw.x_max),REL_Y(todraw.y_max));
     glVertex3f(todraw.x_max,todraw.y_max,todraw.z_max);
     glEnd();

     //left
	 if (glIsTexture(this->m_textures[1]))
		 glBindTexture(GL_TEXTURE_2D,this->m_textures[1]);
	 else
		 glBindTexture(GL_TEXTURE_2D,this->dummytexture);
     glBegin(GL_QUADS);
     glTexCoord2f(REL_XZ(todraw.x_min), REL_Y(todraw.y_max));
     glVertex3f(todraw.x_min,todraw.y_max,todraw.z_min);
     glTexCoord2f(REL_XZ(todraw.x_min), REL_Y(todraw.y_min));
     glVertex3f(todraw.x_min,todraw.y_min,todraw.z_min);
     glTexCoord2f(REL_XZ(todraw.x_max),REL_Y(todraw.y_min));
     glVertex3f(todraw.x_max,todraw.y_min,todraw.z_min);
     glTexCoord2f(REL_XZ(todraw.x_max),REL_Y(todraw.y_max));
     glVertex3f(todraw.x_max,todraw.y_max,todraw.z_min);
     glEnd();

     //top
	 if (glIsTexture(this->m_textures[2]))
		 glBindTexture(GL_TEXTURE_2D,this->m_textures[2]);
	 else
		 glBindTexture(GL_TEXTURE_2D,this->dummytexture);
     glBegin(GL_QUADS);
     glTexCoord2f(REL_XZ(todraw.x_min),REL_XZ(todraw.z_min));
     glVertex3f(todraw.x_min,todraw.y_max,todraw.z_min);
     glTexCoord2f(REL_XZ(todraw.x_min),REL_XZ(todraw.z_max));
     glVertex3f(todraw.x_min,todraw.y_max,todraw.z_max);
     glTexCoord2f(REL_XZ(todraw.x_max),REL_XZ(todraw.z_max));
     glVertex3f(todraw.x_max,todraw.y_max,todraw.z_max);
     glTexCoord2f(REL_XZ(todraw.x_max),REL_XZ(todraw.z_min));
     glVertex3f(todraw.x_max,todraw.y_max,todraw.z_min);
     glEnd();

     //bottom
	 if (glIsTexture(this->m_textures[3]))
		 glBindTexture(GL_TEXTURE_2D,this->m_textures[3]);
	 else
		 glBindTexture(GL_TEXTURE_2D,this->dummytexture);
     glBegin(GL_QUADS);
     glTexCoord2f(REL_XZ(todraw.x_min),REL_XZ(todraw.z_min));
     glVertex3f(todraw.x_min,todraw.y_min,todraw.z_min);
     glTexCoord2f(REL_XZ(todraw.x_min),REL_XZ(todraw.z_max));
     glVertex3f(todraw.x_min,todraw.y_min,todraw.z_max);
     glTexCoord2f(REL_XZ(todraw.x_max),REL_XZ(todraw.z_max));
     glVertex3f(todraw.x_max,todraw.y_min,todraw.z_max);
     glTexCoord2f(REL_XZ(todraw.x_max),REL_XZ(todraw.z_min));
     glVertex3f(todraw.x_max,todraw.y_min,todraw.z_min);
     glEnd();

     //back
	 if (glIsTexture(this->m_textures[4]))
		 glBindTexture(GL_TEXTURE_2D,this->m_textures[4]);
	 else
		 glBindTexture(GL_TEXTURE_2D,this->dummytexture);
     glBegin(GL_QUADS);
     glTexCoord2f(REL_XZ(todraw.z_min),REL_Y(todraw.y_min));
     glVertex3f(todraw.x_min,todraw.y_min,todraw.z_min);
     glTexCoord2f(REL_XZ(todraw.z_max),REL_Y(todraw.y_min));
     glVertex3f(todraw.x_min,todraw.y_min,todraw.z_max);
     glTexCoord2f(REL_XZ(todraw.z_max),REL_Y(todraw.y_max));
     glVertex3f(todraw.x_min,todraw.y_max,todraw.z_max);
     glTexCoord2f(REL_XZ(todraw.z_min),REL_Y(todraw.y_max));
     glVertex3f(todraw.x_min,todraw.y_max,todraw.z_min);
     glEnd();

     //front
	 if (glIsTexture(this->m_textures[5]))
		 glBindTexture(GL_TEXTURE_2D,this->m_textures[5]);
	 else
		 glBindTexture(GL_TEXTURE_2D,this->dummytexture);
     glBegin(GL_QUADS);
     glTexCoord2f(REL_XZ(todraw.z_min), REL_Y(todraw.y_min));
     glVertex3f(todraw.x_max,todraw.y_min,todraw.z_min);
     glTexCoord2f(REL_XZ(todraw.z_max), REL_Y(todraw.y_min));
     glVertex3f(todraw.x_max,todraw.y_min,todraw.z_max);
     glTexCoord2f(REL_XZ(todraw.z_max), REL_Y(todraw.y_max));
     glVertex3f(todraw.x_max,todraw.y_max,todraw.z_max);
     glTexCoord2f(REL_XZ(todraw.z_min), REL_Y(todraw.y_max));
     glVertex3f(todraw.x_max,todraw.y_max,todraw.z_min);
     glEnd();

     glDisable(GL_TEXTURE_2D);
 }

 void GLWidget::drawXYZ() {
	 if (m_bAxisEnabled) {
		 glDisable(GL_TEXTURE_2D);
		 glEnable(GL_BLEND);
		 glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		 glLineWidth(1.5);
		 glColor4f(1.0,0,0,1.0);

		 glBegin(GL_LINES);
		 glVertex3f(-1000,0,0);
		 glVertex3f(0,0,0);
		 glEnd();

		 glColor4f(0.0,0,1.0,1.0);
		 glBegin(GL_LINES);
			 glVertex3f(0,0,0);
			 glVertex3f(1000,0,0);
		 glEnd();

		 glColor4f(1.0,0,0,1.0);
		 glBegin(GL_LINES);
		 glVertex3f(0,-1000,0);
		 glVertex3f(0,1000,0);
		 glEnd();

		 glBegin(GL_LINES);
		 glVertex3f(0,0,-1000);
		 glVertex3f(0,0,1000);
		 glEnd();

		 glDisable(GL_BLEND);
		 glEnable(GL_TEXTURE_2D);
	 }
 }

 void GLWidget::drawFloor() {

	 if (m_bFloorGridEnabled) {
		 glDisable(GL_TEXTURE_2D);
		 glEnable(GL_BLEND);
		 glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		 glLineWidth(1);
		 glColor4f(0.0,1.0,0,0.5);

		 for (int i = (-5*BS);i <= (5*BS); i += BS) {
			 glBegin(GL_LINES);
				  glVertex3f(i,-(BS/2),(-5*BS));
				  glVertex3f(i,-(BS/2),(5*BS));
			 glEnd();

			 glBegin(GL_LINES);
				  glVertex3f((-5*BS),-(BS/2),i);
				  glVertex3f((5*BS),-(BS/2),i);
			 glEnd();
		 }

		 glDisable(GL_BLEND);
		 glEnable(GL_TEXTURE_2D);
	 }
 }

 void GLWidget::paintGL()
 {
     glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
     glPushMatrix();
     glRotated(xRot / 16.0, 1.0, 0.0, 0.0);
     glRotated(yRot / 16.0, 0.0, 1.0, 0.0);
     glRotated(zRot / 16.0, 0.0, 0.0, 1.0);

     //glScalef(m_scale/m_width, m_scale/m_width, m_scale/m_width);
     glScalef(m_scale,m_scale,m_scale);

     drawXYZ();
     drawFloor();

     glColor3f(1.0,1.0,0.0);
     for(QList<box>::iterator i = m_currentBoxes->begin(); i != this->m_currentBoxes->end(); i++) {
    	 drawBox(*i);
     }

     glPopMatrix();
 }

 void GLWidget::resizeGL(int width, int height)
 {
     //int side = qMin(width, height);
     //glViewport((width - side) / 2, (height - side) / 2, side, side);

     m_width = width;
     m_height = height;

     glMatrixMode(GL_PROJECTION);
     glLoadIdentity();
     glViewport(0,0,width,height);
     gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
     //glFrustum(-1.0, +1.0, -1.0, 1.0, 5.0, 60.0);

     glMatrixMode(GL_MODELVIEW);
     glLoadIdentity();
     glTranslated(0.0, 0.0, -40.0);
 }

 void GLWidget::mousePressEvent(QMouseEvent *event)
 {
     lastPos = event->pos();
 }

 void GLWidget::mouseMoveEvent(QMouseEvent *event)
 {
     int dx = event->x() - lastPos.x();
     int dy = event->y() - lastPos.y();

     if (event->buttons() & Qt::LeftButton) {
         setXRotation(xRot + 8 * dy);
         setYRotation(yRot + 8 * dx);
     } else if (event->buttons() & Qt::RightButton) {
         setXRotation(xRot + 8 * dy);
         setZRotation(zRot + 8 * dx);
     }
     lastPos = event->pos();
 }

 void GLWidget::wheelEvent(QWheelEvent *event){
	 event->delta() > 0 ? m_scale += m_scale*0.1f : m_scale -= m_scale*0.1f;
	 updateGL();
 }

 void GLWidget::normalizeAngle(int *angle)
 {
     while (*angle < 0)
         *angle += 360 * 16;
     while (*angle > 360 * 16)
         *angle -= 360 * 16;
 }

 void GLWidget::setBoxes(QList<box>* toset) {
	 this->m_currentBoxes = toset;
 }

 void GLWidget::setSelectedBox(QString name) {
	 m_selected_box = name;
 }

 void GLWidget::setFloorGridEnabled(int state) {

	 if ((state == Qt::Checked) || (state == Qt::PartiallyChecked))
		 m_bFloorGridEnabled = true;
	 else
		 m_bFloorGridEnabled = false;

	 updateGL();
 }

 void GLWidget::setAxisEnabled(int state) {

	 if ((state == Qt::Checked) || (state == Qt::PartiallyChecked))
		 m_bAxisEnabled = true;
	 else
		 m_bAxisEnabled = false;

	 updateGL();
 }
