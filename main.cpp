#include "wielditemmodeleditor.h"

#include <QtGui>
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    WieldItemModelEditor w;
    w.show();
    return a.exec();
}
