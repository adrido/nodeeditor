
 #ifndef __GLWIDGET_H__
 #define __GLWIDGET_H__

 #include <QGLWidget>

#define BS 100

struct box {
	 QString name;
	 double x_min;
	 double y_min;
	 double z_min;

	 double x_max;
	 double y_max;
	 double z_max;
 };

inline double REL_XZ(double pos) {

	while (pos < -(BS/2)) {	pos += BS;	}
	while (pos > +(BS/2)) {	pos -= BS;	}

	return (((BS/2) + pos) / BS);
	}

inline double REL_XZ_INV(double pos)  {
	while (pos < -(BS/2)) {	pos += BS;	}
	while (pos > +(BS/2)) {	pos -= BS;	}

	return (((BS/2) - pos) / BS);
}

inline double REL_Y(double pos) {
	while (pos < -(BS/2)) {	pos += BS;	}
	while (pos > +(BS/2)) {	pos -= BS;	}

	return (((BS/2) + pos ) / BS);
}

 class GLWidget : public QGLWidget
 {
     Q_OBJECT

 public:
     GLWidget(QWidget *parent = 0);
     ~GLWidget();

     int xRotation() const { return xRot; }
     int yRotation() const { return yRot; }
     int zRotation() const { return zRot; }

     void setBoxes(QList<box>* toset);
     void loadTexture(QString filename, int number);
     void setSelectedBox(QString name);


 public slots:
     void setXRotation(int angle);
     void setYRotation(int angle);
     void setZRotation(int angle);
     void setFloorGridEnabled(int state);
     void setAxisEnabled(int state);

 signals:
     void xRotationChanged(int angle);
     void yRotationChanged(int angle);
     void zRotationChanged(int angle);

 protected:
     void initializeGL();
     void paintGL();
     void resizeGL(int width, int height);
     void mousePressEvent(QMouseEvent *event);
     void mouseMoveEvent(QMouseEvent *event);
     void wheelEvent(QWheelEvent *event);

 private:
     void drawBox(box);
     void drawXYZ();
     void drawFloor();

     void normalizeAngle(int *angle);

     int xRot;
     int yRot;
     int zRot;

     QPoint lastPos;

     float m_scale;
     int m_width;
     int m_height;

     GLubyte* dummytexture_data;
     GLuint   dummytexture;

     GLuint   m_textures[6];
     GLubyte* m_texture_data[6];

     QList<box>* m_currentBoxes;

     QString m_selected_box;
     bool m_bFloorGridEnabled;
     bool m_bAxisEnabled;
 };

 #endif
